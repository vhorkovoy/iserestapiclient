﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IseRestApiClient
{
    class SingleLines
    {
        public IDictionary<int, string> SelectedNumbers { get; set; }
        public decimal Winnings { get; set; }
    }
}
