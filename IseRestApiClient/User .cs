﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IseRestApiClient 
{
    class User
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string MemberId { get; set; }

        public string UserSessionId { get; set; }

        public static User LoadFromServiceResponse(string responseMessage) { return JsonConvert.DeserializeObject<User>(responseMessage); }
    }
}
