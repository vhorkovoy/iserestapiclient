﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IseRestApiClient
{
    class TicketsDraws
    {
        public int Id { get; set; }

        public int PivotNumber { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public int LotteryTypeId { get; set; }

        public string LotteryName { get; set; }

        public int DrawId { get; set; }

        public DateTime DrawDate { get; set; }

        public string WinningResult { get; set; }

        public string BonusNumber { get; set; }

        public SingleLines SingleLines { get; set; }

        public decimal Winning { get; set; }
        public List<string> ScanImageUrls { get; set; }

        public bool IsSyndicate { get; set; }

        public bool isFree { get; set; }


        public static List<TicketsDraws> LoadFromServiceResponse(string responseMessage)
        { 
            return JsonConvert.DeserializeObject<List<TicketsDraws>>(responseMessage);
        }
    }
}
