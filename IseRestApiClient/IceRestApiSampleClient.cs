﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace IseRestApiClient
{
    class IceRestApiSampleClient :IDisposable
    {
        HttpClient _httpClient;
       //string _apiUrl = "https://3.124.2.226:444/";
      // string _apiUrl = "https://api1.braviotechnology.com/";
        string _apiUrl = "http://localhost:81/";
        //string _apiUrl = "http://localhost:83/";
        // string _apiUrl = "https://crm.braviotechnology.com:444/";
        int _brandId = 256;
        int _memberID = 2068;


        public IceRestApiSampleClient()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
               (sender, certificate, chain, errors) => true;
            _httpClient = new HttpClient(CreateHandler()); // instantiating             
            _httpClient.DefaultRequestHeaders.Add("Token", "LWG0699w");
        }
        public HttpResponseMessage CheckWebHook()
        {
            var methodUrl = _apiUrl + "cashier/update-payment-status";
            var res = new
            {
                merchantProcessor = "Bitcoin.com",
                merchantName = "Unregistered merchant",
                verification = "UNVERIFIED",
                invoiceCurrency = "USD",
                invoiceAmount = 0.0498276,
                paymentCurrency = "BCH",
                paymentId = "1234567890",
                paymentAmount = 18000,
                conversionRate = 276.82,
                conversionAssets = "USD/BCH",
                itemDesc = "Payment request for invoice FFbFxhisukytSvAwGqZDsD",
                email = "support@bitcoin.com",
                merchantWebsite = "https://www.bitcoin.com",
                createTime = 1567108639293,
                expiryTime = 1567109539293,
                status = "open"
            };
            return _httpClient.PostAsJsonAsync(methodUrl, res).Result;
        }
        public HttpResponseMessage UpdatePaymentStatus()
        {
            var methodUrl = $"{_apiUrl}processorsresponse/update-payment-status";
            var res = new
            {
                status = "paid",
                paymentId = "DeuJss5Bjrm4koz5gmxMho",
            };
            return _httpClient.PostAsJsonAsync(methodUrl, res).Result;
        }
        public string SignupUserWithLogin()
        {
            var methodUrl = _apiUrl + "/signup/session";
            using (HttpResponseMessage response = _httpClient.PostAsJsonAsync(methodUrl, new
            {
                FirstName = "John",
                LastName = "W",
                Email = "mymail@myname.com",
                BrandId = 1,
                IP = "10.10.10.10"
            }).Result)  // Blocking call!             
            {
                return ReturnResponseMessage(response);
            }
        }
        public string GetDrawTicketsByMemberId()
        {
            string methodUrl = _apiUrl + "playlottery/get-draws-tickets-by-memberid"; // signup without session 

            using (var response = _httpClient.PostAsJsonAsync(methodUrl,
                new { BrandId = _brandId, MemberId = _memberID, PageNumber = 1 }).Result)  // Blocking call! - this means that the result blocks the thread until it receives the whole content             
            {
                return ReturnResponseMessage(response);
            }
        }
        public HttpResponseMessage GeAllBrandDraws()
        {
            string methodUrl = _apiUrl + "/globalinfo/get-all-brand-draws"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, new { BrandId = _brandId}).Result; 

            //using (var response = _httpClient.PostAsJsonAsync(methodUrl,
            //    new { BrandId = _brandId, MemberId = _memberID, PageNumber = 1 }).Result)  // Blocking call! - this means that the result blocks the thread until it receives the whole content             
            //{
            //    return ReturnResponseMessage(response);
            //}
        }
        public HttpResponseMessage GetLotteriesResultsByBrand()
        {
            string methodUrl = _apiUrl + "/globalinfo/get-lotteries-results-by-brand"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, new { BrandId = _brandId }).Result;

            //using (var response = _httpClient.PostAsJsonAsync(methodUrl,
            //    new { BrandId = _brandId, MemberId = _memberID, PageNumber = 1 }).Result)  // Blocking call! - this means that the result blocks the thread until it receives the whole content             
            //{
            //    return ReturnResponseMessage(response);
            //}
        }
        public HttpResponseMessage GetLotteriesRules()
        {
            string methodUrl = _apiUrl + "/globalinfo/lottery-rules"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, new { BrandId = _brandId }).Result;

            //using (var response = _httpClient.PostAsJsonAsync(methodUrl,
            //    new { BrandId = _brandId, MemberId = _memberID, PageNumber = 1 }).Result)  // Blocking call! - this means that the result blocks the thread until it receives the whole content             
            //{
            //    return ReturnResponseMessage(response);
            //}
        }
        public HttpResponseMessage GetAvailableSubbrandsForBrand()
        {
            string methodUrl = _apiUrl + "/globalinfo/get-subbrands-for-brand"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, new { BrandId = _brandId }).Result;

            //using (var response = _httpClient.PostAsJsonAsync(methodUrl,
            //    new { BrandId = _brandId, MemberId = _memberID, PageNumber = 1 }).Result)  // Blocking call! - this means that the result blocks the thread until it receives the whole content             
            //{
            //    return ReturnResponseMessage(response);
            //}
        }
        public HttpResponseMessage ConfirmBrandOrder(BrandProductsOrderingRequest request)
        {
            string methodUrl = _apiUrl + "/cashier/confirm-brand-order"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, request).Result;
        }
        public HttpResponseMessage GetTicketDrawsByMemberId()
        {
            var request = new
            {
                BrandId = 256,
                MemberId = 10002,
                PageNumber = 1
            };
            string methodUrl = _apiUrl + "/playlottery/get-draws-tickets-by-memberid"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, request).Result;
        }
        public HttpResponseMessage PrepareOrderCrmOrder()
        {
            var request = new PrepareOrderRequest()
            {
                MemberId = 10002,
                BrandId = 256,
                ProductsArray = new List<ProductEntityCrm>()
            };
            request.ProductsArray.Add(new ProductEntityCrm()
            {
                ProductId = 1,
                LotteryTypeID = 1,
                numOfLines = 2,
                numOfDraws = 2,
                IsCash = true
            });
            string methodUrl = _apiUrl + "/cashiercrm/prepare-order-crm"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, request).Result;
        }
        public HttpResponseMessage ReportLogin()
        {
            var request = new
            {
                BrandId = 256,
                Password = "valerik1",
                Username = "valerik"
            };
            string methodUrl = _apiUrl + "/backoffice/backoffice-login"; // signup without session 
            return _httpClient.PostAsJsonAsync(methodUrl, request).Result;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        private WebRequestHandler CreateHandler()
        {
            return new WebRequestHandler { AllowAutoRedirect = false, PreAuthenticate = false };
        }
        private static string ReturnResponseMessage(HttpResponseMessage response)
        {
            var returnedMessage = response.IsSuccessStatusCode ?
                response.Content.ReadAsStringAsync().Result :
                string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            return returnedMessage;
        }
    }
}
