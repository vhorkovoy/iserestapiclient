﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IseRestApiClient
{
    public class BrandProductsOrderingRequest
    {
        public int BrandId { get; set; }
        public string BrandMeberId { get; set; }
        public string BrandMeberEmailId { get; set; }
        public List<BrandProductRequest> Products { get; set; }
        public BrandProductsOrderingRequest()
        {
            Products = new List<BrandProductRequest>();
        }
    }
    public class BrandProductRequest
    {
        public int Id { get; set; }
        public int LotteryType { get; set; }
        public int NumberOfDraws { get; set; }
        public int NumberOfLines { get; set; }
        public int NumberOfShares { get; set; }
        public List<string> SelectedNumbers { get; set; }
        public BrandProductRequest()
        {
            SelectedNumbers = new List<string>();
        }
    }
    public class PrepareOrderRequest
    {
        public string ProductNumsLottery { get; set; }
        public string ReedemCode { get; set; }
        public int MemberId { get; set; }
        public int BrandId { get; set; }
        public List<ProductEntityCrm> ProductsArray { get; set; }
    }
    public class ProductEntityCrm
    {
        public bool IsFreeProduct { get; set; }

        public int ProductId { get; set; }

        public int LotteryTypeID { get; set; }

        public int numOfLines { get; set; }

        public int numOfDraws { get; set; }

        public bool IsCash { get; set; }

        public bool IsVip { get; set; }

        public int MemberId { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var property in this.GetType().GetProperties())
            {
                sb.Append(property.Name);
                sb.Append("=");
                if (property.GetIndexParameters().Length > 0)
                {
                    sb.Append("Indexed Property cannot be used");
                }
                else
                {
                    sb.Append(property.GetValue(this, null));
                }

                sb.Append("&");
            }

            return sb.ToString().Remove(sb.ToString().Length - 1, 1);
        }
    }
}
